#[macro_use]
extern crate rocket;

use std::collections::HashMap;
use anyhow::Result;
use mysql_async::prelude::*;
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::{Header, Status};
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Request, Response};

use std::env;

pub struct CORS;

#[rocket::async_trait]
impl Fairing for CORS {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to responses",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, GET, PATCH, OPTIONS",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Page {
    title: String,
    namespace: usize,
    bytes: usize,
    last_touched: usize,
}

pub struct Indirect {
    page: Page,
    redirect_page_id: usize,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Redirect {
    title: String,
    namespace: usize,
    to_section: String,
    pages: Vec<Page>,
    #[serde(skip_serializing)]
    page_id: usize, 
}

impl Redirect {
    pub fn new(page_id: usize, title: String, namespace: usize, to_section: String) -> Self {
        Redirect { page_id, title, namespace, to_section, pages: Vec::<Page>::new(), }
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct LinksToPage {
    pages: Vec<Page>,
    redirects: Vec<Redirect>,

    #[serde(skip_serializing)]
    redirect_index_map: HashMap<usize, usize>
}

impl LinksToPage {
    pub fn new() -> Self {
        LinksToPage { pages: Default::default(), redirects: Default::default(), redirect_index_map: Default::default() }
    }
    pub fn add_pages(&mut self, pages: Vec<Page>) {
        self.pages = pages;
    }
    pub fn add_redirects(&mut self, mut redirects: Vec<Redirect>) {
        for redirect in redirects.drain(0..) {
            let i = self.redirects.len();
            self.redirect_index_map.insert(redirect.page_id, i);
            self.redirects.push(redirect);
        }
    }
    pub fn add_indirect_links(&mut self, mut indirects: Vec<Indirect>) {
        for indirect in indirects.drain(0..) {
            if let Some(redirect_index) = self.redirect_index_map.get(&indirect.redirect_page_id) {
                self.redirects[*redirect_index].pages.push(indirect.page);
            } else {
                println!("Warning: unknown redirect page id {}", indirect.redirect_page_id);
            }
        }
    }
}

#[get("/")]
async fn index() -> String {
    return r#"{"TODO": "documentation"}"#.to_string();
}

#[get("/backlinks?<title>&<ns>&<from_ns>")]
async fn get_backlinks(title: &str, ns: Option<usize>, from_ns: Option<&str>) -> Result<Json<LinksToPage>, Status> {
    let to_namespace = if ns.is_some() { ns.unwrap() } else { 0 };
    let mut sanitized_from_ns: Option<String> = None;
    if let Some(list) = from_ns {
        if let Ok(sanitized_numbers) = sanitize_id_list(list) {
            sanitized_from_ns = Some(sanitized_numbers);
        } else {
            return Err(Status::BadRequest); // was not a list of numbers
        }
    }
    let mut links_to_page = LinksToPage::new();
    match get_direct_links(to_namespace, title.to_string(), &sanitized_from_ns).await {
        Ok(pages) => links_to_page.add_pages(pages),
        Err(err) => {
            println!("{}", err);
            return Err(Status::InternalServerError);
        }
    }
    match get_redirects(to_namespace, title.to_string(), &sanitized_from_ns).await {
        Ok(redirects) => links_to_page.add_redirects(redirects),
        Err(err) => {
            println!("{}", err);
            return Err(Status::InternalServerError);
        }
    }
    match get_indirect_links(to_namespace, title.to_string(), &sanitized_from_ns).await {
        Ok(indirects) => links_to_page.add_indirect_links(indirects),
        Err(err) => {
            println!("{}", err);
            return Err(Status::InternalServerError);
        }
    }
    Ok(Json(links_to_page))
}


fn sanitize_id_list(number_list: &str) -> Result<String> {
    let mut numerals = Vec::<usize>::new();
    for maybe_num in number_list.split(",") {
        if let Ok(num) = maybe_num.parse::<usize>() {
            numerals.push(num);
        } else {
            return Err(anyhow::anyhow!("{} is not a number", maybe_num));
        }
    }
    return Ok(numerals.iter().map(|x| x.to_string()).collect::<Vec<_>>().join(","));
}


async fn get_direct_links(namespace: usize, page_name: String, from_ns: &Option<String>) -> Result<Vec<Page>> {
    let mut conn = db_conn().await?;
    let params = params! {
        "namespace" => namespace,
        "name" => page_name,
    };
    let mut query = r#"
        SELECT page_title, page_namespace, page_len, page_touched
        FROM page
        JOIN pagelinks ON pagelinks.pl_from=page.page_id
        WHERE pl_namespace = :namespace
        AND pl_title = :name
    "#.to_string();
    if let Some(from_ns_nums) = from_ns {
        query += &format!("AND pl_from_namespace IN ({})", from_ns_nums);
    }
    query += r#"
        ORDER BY page_namespace, page_title;
    "#;
    let pages = conn
        .exec_map(
            query,
            params,
            |(title, namespace, bytes, last_touched)| Page {title, namespace, bytes, last_touched},
        )
        .await?;
    Ok(pages)
}

async fn get_redirects(namespace: usize, page_name: String, from_ns: &Option<String>) -> Result<Vec<Redirect>> {
    let mut conn = db_conn().await?;
    let params = params! {
        "namespace" => namespace,
        "title" => page_name,
    };
    let mut query = r#"
        SELECT redir_page.page_id, redir_page.page_title, redir_page.page_namespace, rd_fragment
        FROM redirect 
        JOIN page AS redir_page ON redir_page.page_id = rd_from
        WHERE rd_namespace = :namespace
        AND rd_title = :title
    "#.to_string();
    if let Some(from_ns_nums) = from_ns {
        query += &format!("AND redir_page.page_namespace IN ({})", from_ns_nums);
    }
    query += r#"
        ORDER BY redir_page.page_namespace, redir_page.page_title;
    "#;
    let pages = conn
        .exec_map(
            query,
            params,
            |(page_id, title, namespace, to_section)| Redirect::new(page_id, title, namespace, to_section),
        )
        .await?;
    Ok(pages)
}

async fn get_indirect_links(namespace: usize, page_name: String, from_ns: &Option<String>) -> Result<Vec<Indirect>> {
    let mut conn = db_conn().await?;
    let params = params! {
        "namespace" => namespace,
        "title" => page_name,
    };
    let mut query = r#"
        SELECT pl_page.page_title, pl_from_namespace, pl_page.page_len, pl_page.page_touched, redir_page.page_id 
        FROM redirect 
        JOIN page AS redir_page ON redir_page.page_id = rd_from
        JOIN pagelinks ON pl_namespace = rd_namespace AND pl_title = redir_page.page_title
        JOIN page AS pl_page ON pl_from = pl_page.page_id
        WHERE rd_namespace = :namespace
        AND rd_title = :title
    "#.to_string();
    if let Some(from_ns_nums) = from_ns {
        query += &format!("AND pl_from_namespace IN ({})", from_ns_nums);
    }
    query += r#"
        ORDER BY pl_from_namespace, pl_page.page_title;
    "#;
    let pages = conn
        .exec_map(
            query,
            params,
            |(title, namespace, bytes, last_touched, redirect_page_id)| Indirect { page: Page{title, namespace, bytes, last_touched}, redirect_page_id },
        )
        .await?;
    Ok(pages)
}

async fn db_conn() -> Result<mysql_async::Conn> {
    let db_url: String;
    let port = env::var("MYSQL_PORT");
    let user = env::var("MYSQL_USER");
    let pass = env::var("MYSQL_PASS");
    if port.is_ok() && user.is_ok() && pass.is_ok() {
        db_url = format!(
            "mysql://{}:{}@localhost:{}/enwiki_p",
            user.unwrap(),
            pass.unwrap(),
            port.unwrap()
        )
        .to_string();
    } else {
        db_url = toolforge::connection_info!("enwiki_p", WEB)?.to_string();
    }
    let pool = mysql_async::Pool::new(db_url.as_str());
    Ok(pool.get_conn().await?)
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index, get_backlinks])
        .attach(CORS)
}
