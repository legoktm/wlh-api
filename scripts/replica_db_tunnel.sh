#!/usr/bin/env bash

username=${1-$USER}

if [[ -f .tunnel_pid ]] && ps -p $(cat .tunnel_pid) > /dev/null; then
	echo "tunnel already running"
else
	ssh -N $username@dev.toolforge.org -L 3307:enwiki.analytics.db.svc.wikimedia.cloud:3306 &
	echo $! > .tunnel_pid
fi
