#!/usr/bin/env bash

user=${1-$USER}

ssh $user@login.toolforge.org "
  set -ex
  become wlh-api bash -exc \"
    cd ~/www/rust &&
    git pull --ff-only &&
    pwd &&
    time jsub -N build -mem 2G -sync y -cwd cargo build --release &&
    webservice stop &&
    webservice start
  \"
"
